# .bashrc

# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi

# Source global definitions
[[ -f /etc/bashrc ]] && . /etc/bashrc

# User specific environment
if [[ -z "$_USER_RC_COMPLETED" ]]; then
	export LC_CTYPE="en_US.UTF-8"
	export PATH="/usr/sbin:/sbin:$HOME/.local/bin:$HOME/bin:$PATH"
	export HISTSIZE=10000
	export HISTFILESIZE=10000
	export HISTCONTROL="erasedups"
	export HISTIGNORE=$'[ \t]*:&:[bf]g:[bf]g *:j:jobs:history*:l:l[sla]:exit:logout'
	export PROMPT_COMMAND="history -a;history -n;$PROMPT_COMMAND"
fi
export PATH HISTSIZE HISTFILESIZE HISTCONTROL HISTIGNORE PROMPT_COMMAND

# User specific aliases and functions
shopt -s histappend histreedit histverify

export EDITOR="vim"

alias l='ls -CF'
alias ll='ls -l'
alias la='ll -A'
alias j='jobs'

alias tree='tree -C'

# Set colors using tput commands
FGBLK=$( tput setaf  0 ) # 000000
FGRED=$( tput setaf  1 ) # ff0000
FGGRN=$( tput setaf  2 ) # 00ff00
FGYLO=$( tput setaf  3 ) # ffff00
FGBLU=$( tput setaf  4 ) # 0000ff
FGMAG=$( tput setaf  5 ) # ff00ff
FGCYN=$( tput setaf  6 ) # 00ffff
FGWHT=$( tput setaf  7 ) # ffffff
FBBLK=$( tput setaf  8 ) # 000000
FBRED=$( tput setaf  9 ) # ff0000
FBGRN=$( tput setaf 10 ) # 00ff00
FBYLO=$( tput setaf 11 ) # ffff00
FBBLU=$( tput setaf 12 ) # 0000ff
FBMAG=$( tput setaf 13 ) # ff00ff
FBCYN=$( tput setaf 14 ) # 00ffff
FBWHT=$( tput setaf 15 ) # ffffff

BGBLK=$( tput setab  0 ) # 000000
BGRED=$( tput setab  1 ) # ff0000
BGGRN=$( tput setab  2 ) # 00ff00
BGYLO=$( tput setab  3 ) # ffff00
BGBLU=$( tput setab  4 ) # 0000ff
BGMAG=$( tput setab  5 ) # ff00ff
BGCYN=$( tput setab  6 ) # 00ffff
BGWHT=$( tput setab  7 ) # ffffff
BBBLK=$( tput setab  8 ) # 000000
BBRED=$( tput setab  9 ) # ff0000
BBGRN=$( tput setab 10 ) # 00ff00
BBYLO=$( tput setab 11 ) # ffff00
BBBLU=$( tput setab 12 ) # 0000ff
BBMAG=$( tput setab 13 ) # ff00ff
BBCYN=$( tput setab 14 ) # 00ffff
BBWHT=$( tput setab 15 ) # ffffff

RESET=$( tput sgr0 )
BOLDM=$( tput bold )
UNDER=$( tput smul )
REVRS=$( tput rev )

CWD_SHORT='\W'
CWD_LONG='\w'

if [[ $EUID == 0 ]]; then
	SHELL_COLOR=$FGRED
	[[ -z "$SHELL_CWD" ]] && SHELL_CWD=$CWD_SHORT
fi

export PS1="\[$BOLDM\]$SHELL_PREFIX\[${SHELL_COLOR:-$FGGRN}\]\h \[$FGBLU\]${SHELL_CWD:-$CWD_LONG} \\\$ \[$RESET\]"
if [[ -n "$CUSTOM_SOURCE" ]]; then
	source "$CUSTOM_SOURCE"
fi
unset SHELL_PREFIX SHELL_CWD CUSTOM_SOURCE

# source local definitions
[[ -f "$HOME/.bashrc.local" ]] && . "$HOME/.bashrc.local"

export _USER_RC_COMPLETED=1

function subsh ()
{
	local sh_cwd=$CWD_LONG
	local f_env=
	OPTIND=1
	while getopts se: opt; do
		case "$opt" in
			s) sh_cwd="$CWD_SHORT" ;;
			e) f_env="$OPTARG" ;;
			*) return ;;
		esac
	done
	shift $((OPTIND-1))
	[[ "$1" = "--" ]] && shift

	SHELL_PREFIX="\[$FGYLO\](${1:-sub$SHLVL}) " SHELL_CWD=$sh_cwd CUSTOM_SOURCE="$f_env" $SHELL
}

function ipk-inspect () {(
	set -e
	local ipk=$(basename $1)
	local d=${ipk%.ipk}
	cd $(dirname $1)
	if mkdir "$d"; then
		cd "$d"
			ar -xv "../$ipk" > /dev/null && \
			mkdir c d && \
			tar xf control.tar* -C c && \
			tar xf data.tar* -C d && \
			rm control.tar* data.tar* && \
			cat c/control && \
			subsh -s ipk || true
		cd -
		rm -rf "$d"
	fi
)}
