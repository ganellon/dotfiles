import os
import ycm_core
import logging

BASE_FLAGS_COMMON = [
  '-Wall',
  '-Wextra',
]
BASE_FLAGS = {
  'c' : [
    '-std=gnu11',
    '-x', 'c',
  ],
  'c++': [
    '-fexceptions',
    '-std=c++17',
    '-x', 'c++',
  ]
}
HEADER_EXTENSIONS = { '.h': 'c', '.hpp': 'c++', '.hxx': 'c++', '.hh': 'c++' }
SOURCE_EXTENSIONS = { '.c': 'c', '.cpp': 'c++', '.cxx': 'c++', '.cc': 'c++' }
TYPE_PRIO = { 'c': 0, 'c++': 1 } # disallow downgrade of c++ headers to c

def GetInfoForFile(filename):
  basename, extension = os.path.splitext(filename)[0:2]
  if extension in HEADER_EXTENSIONS:
    header_type = HEADER_EXTENSIONS[extension]
    logging.info("YEC::GetInfoForFile: header detected")
    for altext in SOURCE_EXTENSIONS:
      source_type = SOURCE_EXTENSIONS[altext]
      replacement_file = basename + altext
      if os.path.exists(replacement_file):
        logging.info("YEC::GetInfoForFile: %s-family source detected", source_type)
        if TYPE_PRIO[header_type] < TYPE_PRIO[source_type]:
          return BASE_FLAGS_COMMON + BASE_FLAGS[source_type]
        else:
          logging.info("YEC::GetInfoForFile: downgrade from %s to %s not allowd", header_type, source_type)
    logging.info("YEC::GetInfoForFile: no source detected, assuming %s-family header", header_type)
    return BASE_FLAGS_COMMON + BASE_FLAGS[header_type]

  if extension in SOURCE_EXTENSIONS:
    logging.info("YEC::GetInfoForFile: %s-family source detected", SOURCE_EXTENSIONS[extension])
    return BASE_FLAGS_COMMON + BASE_FLAGS[SOURCE_EXTENSIONS[extension]]

  logging.info("YEC::GetInfoForFile: unknown file type")
  return None

def FlagsForFile(filename, **kwargs):
  logging.info("YEC: looking for flags for '%s'", filename)
  return { 'flags': GetInfoForFile(filename), 'do_cache': True }
